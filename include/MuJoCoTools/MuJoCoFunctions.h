/**
 * General helper functions for the MuJoCo classes
 */

#ifndef MUJOCOFUNCTIONS_HPP_
#define MUJOCOFUNCTIONS_HPP_

#include <mujoco/mujoco.h>
#include <Eigen/Dense>

namespace MuJoCo
{

/**
 * Search a few known places for an activation key
 *
 * @param[out] path Path to the key, only if found
 * @return True if key found, false otherwise
 */
bool get_activation_key_path(std::string& path);

/**
 * Turn MuJoCo vector into Eigen vector
 *
 * @param num		mjtNum Array
 * @param size		Size of the array
 */
Eigen::VectorXd mujoco_to_eigen(const mjtNum* num, int size);

/**
 * Copy Eigen vector to MuJoCo vector
 *
 * @param res		Pointer to resulting array
 * @param vec		Eigen vector
 * @param n			Size of vector (optional, only for assert)
 */
void eigen_copy(mjtNum* res, const Eigen::VectorXd& vec, int n = 0);

/**
 * Perform multiplication with pseudo-inverse of matrix
 *
 * Moore-Penrose inverse is used.
 *
 * @param A		Matrix to be inverted
 * @param b		Vector to be multiplied with
 */
Eigen::VectorXd psuedo_inverse_mult(Eigen::MatrixXd A, Eigen::VectorXd b);

/**
 * Convert Z, Y', X'' Euler angles to quaternion
 *
 * @param x		X'' angle
 * @param y		Y' angle
 * @param z		Z angle
 */
Eigen::Vector4d euler_to_quaternion(double x, double y, double z);

/**
 * Convert Z, Y', X'' Euler angles to quaternion
 *
 * @param euler 		(X, Y, Z) Euler angles
 */
Eigen::Vector4d euler_to_quaternion(const Eigen::Vector3d& euler);

/**
 * Convert quaternion to Z, Y', X'' Euler anlges
 */
Eigen::Vector3d quaternion_to_euler(const Eigen::Vector4d& quat);

} // namespace MuJoCo

#endif /* MUJOCOFUNCTIONS_HPP_ */
