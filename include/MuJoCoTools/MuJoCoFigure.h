#ifndef MUJOCOFIGURE_H_
#define MUJOCOFIGURE_H_

#include <string>
#include <vector>
#include <mujoco/mujoco.h>
#include <mujoco/glfw3.h>

/**
 * Figure (graph) to be shown inside MuJoCoViewer
 *
 * One figure corresponds to one graph. One figure can consist
 * of multiple lines (curves).
 *
 * Figure object needs to be linked once to MuJoCoViewer using
 * MuJoCoViewer.add_figure().
 */
class MuJoCoFigure
{
public:

	/** Default consstructor */
	MuJoCoFigure();

	/**
	 * Constructor
	 *
	 * @param name		Title of the figure
	 * @param n_lines	Number of lines in figure
	 */
	MuJoCoFigure(const std::string& name, int n_lines = 1);

	virtual ~MuJoCoFigure() = default;

	/** Return viewport */
	mjrRect get_viewport() const;

	/**
	 * @name Set legends
	 */
	///@{
	/**
	 * @param legends 	Set list of legends
	 */
	MuJoCoFigure& set_legends(const std::vector<std::string>& legends);

	/**
	 * @param legend	Set a single legend
	 */
	MuJoCoFigure& set_legend(const std::string& legend);
	///@}

	/** Set label for x-axis */
	MuJoCoFigure& set_x_label(const std::string& label);

	/** Set figure width and height */
	MuJoCoFigure& set_size(int width, int height);

	/** Set figure screen position */
	MuJoCoFigure& set_pos(int x, int y);

	/** Return mjFigure pointer */
	mjvFigure* get_figure();

	/** Get screen position */
	void get_pos(int& x_res, int& y_res);

	/** Get screen size */
	void get_size(int& width_res, int& height_res);

	/** Clear figure entirely */
	void clear();

	/**
	 * @name Set new values in figure
	 *
	 * @param x		Current time (x-axis value)
	 */
	///@{
	/**
	 * @brief Set a single value
	 *
	 * @param x
	 * @param val		New value
	 * @param line_id	Index of line (< n_lines)
	 */
	void add_value(double x, double val, int line_id = 0);

	/**
	 * @brief Set values for multiple lines
	 *
	 * @param x
	 * @param values	Array of new values
	 * @param n_values	Size of array
	 */
	void add_values(double x, const double* values, int n_values = -1);

	///@}

private:
	mjvFigure figure_; 		///< mjFigure object
	int num_points_; 		///< Number of points in the graph per line
	int num_lines_;			///< Number of lines in this figure
	int width_, height_;	///< Figure dimensions in pixels
	int x_, y_;				///< Bottom left corner of figure, in pixels
};

#endif /* MUJOCOFIGURE_H_ */
