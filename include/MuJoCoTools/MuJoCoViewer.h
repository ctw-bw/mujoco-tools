#ifndef MUJOCOVIEWER_H_
#define MUJOCOVIEWER_H_

#include <vector>
#include <chrono>
#include <thread>

#include "mujoco/mujoco.h"
#include "mujoco/glfw3.h"

#include "MuJoCoModel.h"
#include "MuJoCoFigure.h"

// Use forward declaration so we can return it from itself
class MuJoCoViewer;

/**
 * Helper class to render a MuJoCo model
 */
class MuJoCoViewer
{
public:

	/**
	 * Constructor
	 *
	 * @param m 	Model to be linked to
	 */
	MuJoCoViewer(const mjModel* m = nullptr);

	/**
	 * Constructor	Model to be linked to
	 *
	 * @param model
	 */
	MuJoCoViewer(const MuJoCoModel& model);

	virtual ~MuJoCoViewer();

	/** Return true when window was closed */
	bool window_should_close() const;

	/**
	 * Render new scene and poll for glwfw events
	 *
	 * Leave d empty to not update the scene.
	 *
	 * @param d 		New data
	 */
	void update(mjData* d = nullptr);

	/**
	 * Run simulation until window is closed
	 *
	 * Function steps through simulation and displays it, running
	 * in real-time.
	 */
	void play(mjData* d);

	/** Update scene with mjData */
	void update_scene(mjData* d);

	/**
	 * Initialize viewer with model
	 *
	 * Typically only called from inside the constructor.
	 */
	void initialize(const mjModel* m);

	/**
	 * @name Callback methods
	 *
	 * Methods are called from inside the window.
	 */
	///@{
	static void cb_keyboard(GLFWwindow* window, int key, int scancode, int act,
			int mods);

	static void cb_mouse_button(GLFWwindow* window, int button, int act,
			int mods);

	static void cb_mouse_move(GLFWwindow* window, double xpos, double ypos);

	static void cb_scroll(GLFWwindow* window, double xoffset, double yoffset);
	///@}

	/** Get original object reference which was tucked inside the window */
	static MuJoCoViewer* get_viewer_ptr(GLFWwindow* window);

	/**
	 * Create arrow from the given vector, starting from the base
	 *
	 * Arrow will only show up if added after scene update!
	 *
	 * @param vector		Vector itself
	 * @param base			Location of the base
	 */
	void draw_arrow(const double vector[3], const double base[3]);

	/**
	 * Add new figure
	 *
	 * When the figure does not have an explicit position, it will
	 * be place above the previous figure.
	 *
	 * @param figure	Pointer to existing figure
	 */
	void add_figure(MuJoCoFigure* figure);

	/**
	 * Let current thread wait for an amount of seconds
	 *
	 * @param seconds		Time in seconds
	 */
	static void sleep_seconds(double seconds);

	/** Mouse buttons */
	bool button_left_, button_middle_, button_right_;

	/** Last known mouse position */
	double mouse_x_, mouse_y_;

	/** Pointer to MJ model object */
	const mjModel* m_;

	/** MJ camera object */
	mjvCamera cam_;

	/** MJ visualisations options */
	mjvOption opt_;

	/** MJ scene */
	mjvScene scn_;

	/** MJ GPU context */
	mjrContext con_;

private:
	static bool initialized;	///< Whether any viewer was initialized

	GLFWwindow* window_;		///< Pointer to GL window

	std::vector<MuJoCoFigure*> figures_;	///< List of pointers to linked figures

};

#endif /* MUJOCOVIEWER_H_ */
