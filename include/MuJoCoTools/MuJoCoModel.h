#ifndef MUJOCOMODEL_HPP_
#define MUJOCOMODEL_HPP_

#include <mujoco/mujoco.h>
#include <string>
#include <vector>
#include <utility>
#include <Eigen/Dense>

#include "MuJoCoFunctions.h"

/**
 * @brief Convenient class to interface with MuJoCo models
 *
 * Includes numerical gradients.
 *
 * A model instance is created from an XML file. The mjModel* and
 * mjData* properties are stored internally.
 */
class MuJoCoModel
{
public:
	using MatrixXd = Eigen::MatrixXd;
	using VectorXd = Eigen::VectorXd;
	using Vector3d = Eigen::Vector3d;
	using Vector4d = Eigen::Vector4d;
	using ListVector3d = std::vector<std::pair<int, Eigen::Vector3d>>;
	using ListVector4d = std::vector<std::pair<int, Eigen::Vector4d>>;
	using ListVectorXd = std::vector<std::pair<int, Eigen::VectorXd>>;

	enum DIFF_TYPE
	{
		DIFF_POS, DIFF_VEL, DIFF_TORQUE
	};

	enum FRC_TYPE
	{
		FRC_FORCE = 0, FRC_TORQUE = 1
	};

	/**
	 * @brief Struct to group settings for inverse kinematics
	 *
	 * Used in MuJoCoModel.inverse_kinematics().
	 */
	struct IKSettings
	{
		int iter_max;			///< Iterations limit
		double pos_eps;			///< Step size for position
		double quat_eps;		///< Step size for quaternions
		double pos_threshold;	///< Acceptable position error (norm2)
		double quat_threshold;	///< Acceptable quaternion error (norm2)

		IKSettings();
		IKSettings(int i_max, double pos_eps, double quat_eps,
				double pos_threshold, double quat_threshold);
	};

	/** Regular constructor */
	MuJoCoModel(const std::string& model_file);

	/**
	 * Copy constructor
	 *
	 * Current pose and data will NOT be copied! Only the model
	 * is copied and new data is initialized.
	 */
	MuJoCoModel(const MuJoCoModel& rhs);

	/** Destructor */
	virtual ~MuJoCoModel();

	/**
	 * Assignment operator
	 *
	 * `this` is the left-hand side, `rhs` is the right-hand side.
	 * If right-hand side has a different file, mjModel will be
	 * copied. mjData will always be copied.
	 */
	MuJoCoModel& operator=(const MuJoCoModel& rhs);

	// --------------------------------------------
	/**
	 * @name General methods
	 */
	///@{
	/** Return XML file used to construct model */
	const std::string& get_file() const;

	/**
	 * Return number of bodies in model
	 *
	 * This includes the worldbody!
	 */
	int get_mumber_of_bodies() const;

	/**
	 * Set current state of MJ data object
	 *
	 * Each cartesian force is stacked like [Fx; Fy; Fz; taux; tauy; tauz];
	 * All forward functions will be called after setting states.
	 *
	 * @param qpos		Generalized positions
	 * @param qvel		Generalized velocities
	 * @param u			Actuator forces/torques
	 * @param xfrc		Cartesian applied foces
	 */
	void reset_data(const VectorXd& qpos = VectorXd(0), const VectorXd& qvel =
			VectorXd(0), const VectorXd& u = VectorXd(0),
			const ListVectorXd& xfrc = { });

	/**
	 * Get joint limits corresponding to qpos vector
	 *
	 * Joints without limits will return (-1.0e19, 1.0e19)
	 *
	 * @return Vector of pairs of doubles
	 */
	std::vector<std::pair<double, double>> get_joint_limits() const;

	/**
	 * Get actuator limits
	 *
	 * Unlimited actuators have bounds (-1.0e19, 1.0e19)
	 *
	 * @return Vector of pairs of double
	 */
	std::vector<std::pair<double, double>> get_torque_limits() const;

	/** Return list of quaternion ids */
	const std::vector<int>& get_quat_ids() const;

	/** Get pointer to MJ model object */
	mjModel* get_model() const;

	/** Get pointer to MJ data object */
	mjData* get_data() const;

	/**
	 * Return body id
	 *
	 * @param name		Name of the body as defined in XML
	 */
	int get_body_id(const std::string& name) const;

	/** Return default value for qpos */
	VectorXd get_default_qpos() const;

	/**
	 * Enable or disable contact dynamics
	 *
	 * Modify MuJoCo option flag.
	 */
	void enable_contact(bool enable_contact = true);

	//@}

	// --------------------------------------------
	/**
	 * @name Dynamics functions
	 */
	///@{
	/** Get forward dynamics */
	VectorXd get_dynamics() const;

	/**
	 * @name Get acceleration jacobians
	 *
	 * These jacobians are found through finite difference
	 */
	///@{
	/**
	 * Get derivative of acceleration w.r.t. position
	 *
	 * @return Matrix, size is nv x nq.
	 */
	MatrixXd get_dynamics_diff_qpos();

	/**
	 * Get derivative of acceleration w.r.t. velocity
	 *
	 * @return Matrix, size is nv x nv.
	 */
	MatrixXd get_dynamics_diff_qvel();

	/**
	 * Get derivative of acceleration w.r.t. input force
	 *
	 * @return Matrix, size is nv x nu.
	 */
	MatrixXd get_dynamics_diff_u();

	/**
	 * Get derivative of acceleration w.r.t. cartesian forces
	 *
	 * @return Matrix, size is nv x (nbodies * 6).
	 */
	MatrixXd get_dynamics_diff_xfrc();

	/**
	 * Get derivative of acceleration w.r.t. a segment of cartesian forces
	 *
	 * @param body_id	The body_id starts at 0, which is the world body.
	 * @param type		Either force or torque
	 * @return Matrix, size is nv x 3
	 */
	MatrixXd get_dynamics_diff_xfrc(int body_id, FRC_TYPE type);

	///@}

	/**
	 * Use inverse dynamics to compute generalized force
	 *
	 * The resulting force has dimension nv, it does not relate
	 * to actuator forces u.
	 */
	VectorXd get_inverse_dynamics(const VectorXd& qacc_ref);

	/**
	 * Get sum of all contact forces on a specified body
	 *
	 * Force is in global frame
	 */
	Vector3d get_contact_force(int body_id);

	///@}

	// --------------------------------------------
	/**
	 * @name Kinematics functions
	 *
	 * The body id refers to the MuJoCo m->bodyid.
	 * Note that this start at 0 for the worldbody.
	 *
	 * @param body_id 	Id of MuJoCo body
	 */
	///@{
	/** Get global 3D position of body (at the body frame) */
	Vector3d get_position(int body_id) const;

	/** Get global orientation (quaternion) of body */
	Vector4d get_orientation(int body_id) const;

	/**
	 * Get geometric jacobian of body for position
	 *
	 * This is the velocity jacobian:
	 * \f[
	 *  	\dot{p} = J_v * qvel
	 * \f]
	 */
	MatrixXd get_geometric_jacobian_pos(int body_id) const;

	/**
	 * Get geometric jacobian of body for orientation
	 *
	 * This is the velocity jacobian:
	 * \f[
	 * 		\dot{p} = J_v * qvel
	 * \f]
	 *
	 * Meaning it should be multiplied with angular velocity, not quaternion
	 * rates!
	 */
	MatrixXd get_geometric_jacobian_rot(int body_id) const;

	/**
	 * Get derivative of body position to qpos using finite difference
	 *
	 * This is the position jacobian:
	 * \f[
	 * 		\dot{p} = J_q * \dot{qpos}
	 * \f]
	 */
	MatrixXd get_position_diff_qpos(int body_id);

	// --------------------------------------------
	/**
	 * @name Set body pose through inverse kinematics
	 *
	 * Solver works iteratively by setting end-effector velocities.
	 * This method modifies d->qpos and d->qvel directly.
	 *
	 * @return True of acceptable solution was found within max. iterations
	 */
	///@{
	/**
	 * @brief IK for a single base and end-effector position
	 *
	 * @param base_id 	Body id of the base
	 * @param base_pos	Desired position of the base
	 * @param ee_id		Body id of the end-effector
	 * @param ee_pos	Desired position of the end-effector
	 */
	bool inverse_kinematics(int base_id, const Vector3d& base_pos, int ee_id,
			const Vector3d& ee_pos);

	/**
	 * @brief IK for a list of bodies, for positions and quaternions
	 *
	 * Each reference is a pair of the body id and the desired pose.
	 *
	 * @param refs_pos 		Body ids and their desired location
	 * @param refs_quat		Body ids and their desired orientation (quaternions)
	 * @param settings		IK settings
	 */
	bool inverse_kinematics(const ListVector3d& refs_pos,
			const ListVector4d& refs_quat = { }, const IKSettings& settings =
					IKSettings());
	///@}

	///@}

	// --------------------------------------------
	/**
	 * @name Actuation functions
	 */
	///@{
	/**
	 * Get torque from inputs in generalized coordinates
	 *
	 * This is different from the actuation torque d->u itself.
	 *
	 * @return Generalized actuation torque, size nv x 1
	 */
	VectorXd get_actuation_torque() const;

	/**
	 * Get jacobian of input torques to generalized torques
	 *
	 * Jacobian is built numerically. It should be constant.
	 *
	 * @return Jacobian, size nv x nu
	 */
	MatrixXd get_actuation_jacobian();

	///@}

	// --------------------------------------------
	/**
	 * @name Modelling functions
	 */
	///@{
	/**
	 * Return map of heightfield
	 *
	 * @param id			Hfield id
	 * @param shape_only	Do not include data
	 * @return				Matrix of map (absolute values)
	 */
	MatrixXd get_heightfield(int id, bool shape_only = false);

	/**
	 * @name Set height data of an existing heightfield
	 *
	 * The base_z value is the padding between the bottom of the map
	 * and the bottom of the geom. It will protrude negatively, such that
	 * z = 0 will be global z = 0 too.
	 *
	 * @param map		Matrix of height data
	 * @param radius_x	Half of the x-length
	 * @param radius_y	Half of the y-length
	 * @param base_z	Thickness of the material below z=0
	 * @return bool		True on no errors
	 */
	///@{
	/**
	 * @brief Set normalized height data of an existing heightfield
	 *
	 * Map data must be scaled to [0 1]!
	 *
	 * @param id				ID of the heightfield
	 * @param map
	 * @param max_elevation		Max. height of hfield
	 * @param radius_x
	 * @param radius_y
	 * @param base_z
	 */
	bool set_heightfield_normalized(int id, const MatrixXd& map,
			double max_elevation = 0.0, double radius_x = 0.0, double radius_y =
					0.0, double base_z = 0.0);

	/**
	 * Set absolute height data of an existing heightfield
	 *
	 * Map data is in height, [0, >]
	 * No negative values are allowed!
	 *
	 * @param id		ID of the heightfield
	 * @param map
	 * @param radius_x
	 * @param radius_y
	 * @param base_z
	 */
	bool set_heightfield(int id, MatrixXd map, double radius_x = 0.0,
			double radius_y = 0.0, double base_z = 0.0);

	/**
	 * Set map to geom
	 *
	 * Targeted geom must already have a heightfield ready.
	 * See set_heightmap()
	 *
	 * @param id		ID of the geom (not the heightfield!)
	 * @param map
	 * @param radius_x
	 * @param radius_y
	 * @param base_z
	 */
	bool set_terrain(int id, MatrixXd map, double radius_x = 0.0,
			double radius_y = 0.0, double base_z = 0.0);

	///@}

	///@}

private:
	/**
	 * Take finite difference derivative of qacc to some target
	 *
	 * Use the skipstage argument to skip a stage to speed up computation.
	 *
	 * Result is stacked like:
	 *
	 * qacc_diff = [
	 * 		d qacc1 / d x1
	 * 		d qacc2 / d x1
	 * 		d qacc3 / d x1
	 * 		...
	 * 		d qacc1 / d x2
	 * 		d qacc2 / d x2
	 * 		d qacc3 / d x2
	 * 		...
	 * 	]
	 */
	void finite_difference_dynamics(double* target, int var_size,
			DIFF_TYPE type, double* qacc_diff);

	/**
	 * Return whether a given qpos index is part of a quaternion
	 *
	 * Returned value is the starting index of quaternion or -1 if
	 * not part of a quaternion.
	 *
	 * @param i_q	Index in qpos
	 * @return 		Index of qpos which which is the start of quaternion
	 */
	int get_quaternion_index(int i_q) const;

	/** Turn row-major matrix into a column-major matrix (as arrays) */
	void rowMmjor_to_colmajor(const double* mat_rm, double* mat_cm, int rows,
			int cols) const;

	/** Keep track of how many registered instances this class has */
	static int activated;

	/** MuJoCo model information (loaded once) */
	mjModel* m_;

	/** MuJoCo simulation data (represents a single frame) */
	mjData* d_;

	/** Center output, class property to only allocate once */
	double* qacc_center_;

	/** Simulation warmstart, class property to only allocate once */
	double* qacc_warmstart_;

	/** Finite-difference epsilon */
	double eps_;

	/** First indices of qpos belonging to quaternions */
	std::vector<int> quat_ids_;

	/** Path to the XML file that was loaded */
	std::string file_;
};

#endif /* MUJOCOMODEL_HPP_ */
