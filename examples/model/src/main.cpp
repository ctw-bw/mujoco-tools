#include <iostream>

#include <MuJoCoTools/MuJoCoModel.h>
#include <MuJoCoTools/MuJoCoViewer.h>

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        std::cerr << "Specify an xml model" << std::endl;
        return 10;
    }
    
	MuJoCoModel model(argv[1]);
	
	MuJoCoViewer viewer(model.get_model());
	
	viewer.play(model.get_data()); // Hold until window was closed

	return 0;
}
