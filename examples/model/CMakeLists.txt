# Minimum cmake version
cmake_minimum_required(VERSION 3.0)

# Define project
project(mujoco_tools_demo)

# Get FindOpenGL to prefer legacy library
set(OpenGL_GL_PREFERENCE LEGACY)
if(NOT OpenGL_GL_PREFERENCE)
  set(OpenGL_GL_PREFERENCE LEGACY)
endif()

# Find dependencies
find_package(Eigen3 REQUIRED)
find_package(mujoco REQUIRED)
find_package(OpenGL REQUIRED)
find_package(MuJoCoTools REQUIRED)

add_executable(mujoco_tools_demo src/main.cpp)

target_include_directories(mujoco_tools_demo PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)

target_link_libraries(mujoco_tools_demo PUBLIC MuJoCoTools)
