.. MuJoCo Tools documentation master file, created by
   sphinx-quickstart on Thu Jan 21 12:33:20 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MuJoCo Tools's documentation!
========================================

MuJoCo Tools is a library containing modern C++ style objects to wrap around MuJoCo.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   self
   examples.md
   api/model
   api/viewer
   api/functions



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
