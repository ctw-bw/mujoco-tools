#define EXAMPLE false

#if EXAMPLE

#include <iostream>
#include <chrono>
#include <Eigen/Dense>
#include <cmath>
#include <unistd.h>

#include <mujoco/mujoco.h>
#include <MuJoCoTools/MuJoCoModel.h>
#include <MuJoCoTools/MuJoCoViewer.h>

using namespace std;

int main()
{
	MuJoCoModel model(
			"/home/robert/eclipse/ipopt-workspace/Gambol/src/Robots/five_link_biped_3d.xml");

	Eigen::VectorXd qpos = model.get_default_qpos();

	model.reset_data(qpos);

	const mjModel* m = model.get_model();
	mjData* d = model.get_data();

	Eigen::MatrixXd terrain(10, 10);
	for (int i = 0; i < terrain.size(); i++)
	{
		terrain(i) = static_cast<double>(rand()) / static_cast<double>(RAND_MAX) - 1.0;
	}

	model.set_heightfield(0, terrain, 5.0, 5.0, 1.0);

	MuJoCoViewer viewer(m);

	viewer.play(d);

	return 1;
}

/*
 int main()
 {
 MuJoCoModel model(
 "/home/robert/eclipse/ipopt-workspace/Gambol/src/Robots/five_link_biped_3d.xml");

 Eigen::VectorXd qpos = model.get_default_qpos();

 model.reset_data(qpos);

 auto t_start = chrono::high_resolution_clock::now();

 const int count = 1000;
 for (int i = 0; i < count; i++)
 {
 Eigen::VectorXd qvel = Eigen::VectorXd::Random(model.getModel()->nv);
 model.reset_data(qpos, qvel);
 Eigen::MatrixXd dyn_jac = model.get_dynamics_diff_qvel();
 }

 auto t_end = chrono::high_resolution_clock::now();

 double t_ms =
 static_cast<double>(chrono::duration_cast<chrono::microseconds>(
 t_end - t_start).count()) / 1000.0;

 cout << endl << "Time taken: " << t_ms << " ms" << endl;

 return 1;
 }
 */

/*
 int main()
 {
 MuJoCoModel model(
 "/home/robert/eclipse/ipopt-workspace/Gambol/src/Robots/five_link_biped_3d.xml");

 mjModel* m = model.getModel();
 mjData* d = model.getData();

 m->opt.gravity[2] = 0.0;

 Eigen::VectorXd qpos = model.get_default_qpos();
 Eigen::VectorXd qvel = Eigen::VectorXd::Zero(m->nv);
 Eigen::VectorXd u = Eigen::VectorXd::Zero(m->nu);

 qpos(2) = 1.0;
 qpos.segment(3, 4) = MuJoCo::eulerToQuaternion(M_PI_2, 0.0, 0.0);
 qvel(5) = 3.0;

 //u[0] = 1.0;

 model.reset_data(qpos, qvel, u);

 Eigen::VectorXd qfrc = model.get_actuation_torque();
 std::cout << "qfrc: " << qfrc.transpose() << std::endl;

 Eigen::MatrixXd jac_a = model.get_actuation_jacobian();
 std::cout << "jac_a: " << std::endl << jac_a << std::endl;

 MuJoCoViewer viewer(m);

 viewer.cam.distance = 4.0;

 double simstart = glfwGetTime();

 // Repeat simulation
 while (!viewer.windowShouldClose())
 {
 // Record cpu time at start of iteration
 double tmstart = glfwGetTime();

 viewer.update(d); // Render

 while (d->time < (tmstart - simstart))
 {
 mj_step(m, d);
 }

 // Wait until a total of 1 frame time has passed
 while (glfwGetTime() - tmstart < 1.0 / 60.0)
 {
 usleep(500); // Sleep for 0.5 millisecond to prevent a busy wait
 }
 }

 return 1;
 }
 */

/*
 int main()
 {
 MuJoCoModel model(
 "/home/robert/Master Assignment/MuJoCo/Models/monoped.xml");

 mjData* d = model.getData();
 mjModel* m = model.getModel();

 auto qpos = model.get_default_qpos();
 model.reset_data(qpos);

 MuJoCoViewer viewer(m);

 viewer.update(d);

 MuJoCoViewer::sleep_seconds(1.0);

 viewer.cam.distance = 4.0;

 MuJoCoFigure figure1("sin(t)", 2);
 MuJoCoFigure figure2("cos(t)");

 viewer.addFigure(&figure1);
 viewer.addFigure(&figure2);

 figure1.setLegends( { "sin", "2*sin" });

 double simstart = glfwGetTime();

 // Repeat simulation
 while (!viewer.windowShouldClose())
 {
 // Record cpu time at start of iteration
 double tmstart = glfwGetTime();

 double t = d->time;

 double array[2] = { sin(t), 2.0 * sin(t) };
 figure1.addValues(t, array, 2);

 figure2.addValue(t, cos(t));

 viewer.update(d); // Render

 while (d->time < (tmstart - simstart))
 {
 mj_step(m, d);
 }

 // Wait until a total of 1 frame time has passed
 while (glfwGetTime() - tmstart < 1.0 / 60.0)
 {
 MuJoCoViewer::sleep_seconds(0.1 / 100.0);
 }
 }

 return 1;
 }
 */

#endif
