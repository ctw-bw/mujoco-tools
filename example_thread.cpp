#define EXAMPLE_THREAD true

#if EXAMPLE_THREAD

#include <iostream>
#include <chrono>
#include <Eigen/Dense>
#include <cmath>
#include <thread>
#include <mutex>
#include <memory>

#include <mujoco/mujoco.h>
#include <MuJoCoTools/MuJoCoModel.h>
#include <MuJoCoTools/MuJoCoViewer.h>

std::shared_ptr<MuJoCoViewer> viewer;
std::mutex mux_viewer;

/**
 * Update MuJoCo viewer
 *
 * Function is designed to run from a thread.
 * It will only return when the window was closed.
 */
void update_viewer(const mjModel* m, mjData* d)
{
	mux_viewer.lock();
	viewer = std::make_shared<MuJoCoViewer>(m); // Viewer needs to be created inside thread
	mux_viewer.unlock();

	while (!viewer->window_should_close())
	{
		mux_viewer.lock(); // Block until available

		viewer->update(d);

		mux_viewer.unlock();

		MuJoCoViewer::sleep_seconds(1.0 / 60.0);
	}
}


int main()
{
	MuJoCoModel model(
			"/home/robert/eclipse/ipopt-workspace/Gambol/src/Robots/five_link_biped_3d.xml");

	Eigen::VectorXd qpos = model.get_default_qpos();

	model.reset_data(qpos);

	const mjModel* m = model.get_model();
	mjData* d = model.get_data();

	// Launch thread to update viewer asynchronously
	std::thread t_viewer(update_viewer, m, d);

	while (!viewer)
	{
		// Hold until creation finished
		MuJoCoViewer::sleep_seconds(0.1);
	}

	// Add figure to viewer
	auto fig = std::make_shared<MuJoCoFigure>("My Fig");

	mux_viewer.lock();

	viewer->add_figure(fig.get());

	mux_viewer.unlock();

	t_viewer.join();

	return 1;
}

#endif
