# MuJoCo Tools #

[![Documentation Status](https://readthedocs.org/projects/mujoco-tools/badge/?version=latest)](https://mujoco-tools.readthedocs.io/en/latest/?badge=latest)

This repository contains a few classes that wrap around MuJoCo.

The goal is to make simulation and rendering much easier, with the addition of some useful features like built-in finite-difference.

Code documentation can be found here: <https://mujoco-tools.readthedocs.io/en/latest/>

## Install ##

### MuJoCo ###

The first step is installing MuJoCo. Since MuJoCo comes as a pre-compiled library without a build system, there is no uniform installation. Use this cmake wrapper: https://bitbucket.org/ctw-bw/mujoco

Follow the instructions there to create a cmake install of MuJoCo.

### Build ###

With MuJoCo in place we can start building. From the root of the MuJoCoTools repository:

* `mkdir build && cd build`
* `cmake .. -DCMAKE_BUILD_TYPE=Release`
* `make`
* `sudo make install`

If you want to uninstall MuJoCoTools, run `sudo xargs rm < install_manifest.txt`.

## Basic Usage ##

A basic simulation is now as simple as:

```
#include <MuJoCoTools/MuJoCoModel.h>
#include <MuJoCoTools/MuJoCoViewer.h>

int main()
{
	MuJoCoModel model("/path/to/model.xml");
	
	MuJoCoViewer viewer(model.getModel());
	
	viewer.play(); // Hold until window was closed

	return 0;
}
```

## Documentation

Documentation is generated through Sphinx (with Doxygen and breathe):

```shell
cd docs
make html
```

Generated files can be found in `docs/_build/html/index.html`.

All documentation is placed inside headers such that they are easily accessible after a `make install`, when source .cpp are not easily accessible.

The Sphinx documentation is automatically build and hosted on Read-the-Docs.
