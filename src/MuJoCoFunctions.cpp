#include <MuJoCoTools/MuJoCoFunctions.h>

#include <cassert>
#include <vector>
#include <fstream>

namespace MuJoCo
{

// Search for activation key
bool get_activation_key_path(std::string& key_path) {

    std::vector<std::string> paths = {
            "~/.mujoco/mjkey.txt",
            "/usr/local/lib/mujoco/mjkey.txt",
            "C:/Program Files/mujoco/mjkey.txt",
            "~/mjkey.txt",
            "mjkey.txt",
    };

    for (const std::string& path : paths)
    {
        std::ifstream file(path);
        if (file.good())
        {
            key_path = path;
            return true;
        }
    }

    key_path = paths[0]; // Write the preferred location anyway
    return false;
}

// Turn MuJoCo vector into Eigen vector
Eigen::VectorXd mujoco_to_eigen(const mjtNum* num, int size)
{
	Eigen::VectorXd vec(size);
	mju_copy(vec.data(), num, size);
	return vec;
}

// Copy Eigen vector to MuJoCo vector
void eigen_copy(mjtNum* res, const Eigen::VectorXd& vec, int n)
{
	if (n > 0)
	{
		assert(
				vec.size() == n
						&& "Source and destination do not have the same size");
	}

	mju_copy(res, vec.data(), vec.size());
}

// Perform multiplication with pseudo-inverse of matrix
Eigen::VectorXd psuedo_inverse_mult(Eigen::MatrixXd A, Eigen::VectorXd b)
{
	return (A.transpose() * A).ldlt().solve(A.transpose() * b);
}

// Convert Z, Y', X'' Euler angles to quaternion
Eigen::Vector4d euler_to_quaternion(double x, double y, double z)
{
	using namespace Eigen;

	Quaterniond q;
	q = AngleAxisd(z, Vector3d::UnitZ())
			* AngleAxisd(y, Vector3d::UnitY())
			* AngleAxisd(x, Vector3d::UnitX());

	Vector4d quat(q.w(), q.x(), q.y(), q.z());

	return quat;
}

// Convert Z, Y', X'' Euler angles to quaternion
Eigen::Vector4d euler_to_quaternion(const Eigen::Vector3d& euler)
{
	return euler_to_quaternion(euler[0], euler[1], euler[2]);
}

// Convert quaternion to Z, Y', X'' Euler anlges
Eigen::Vector3d quaternion_to_euler(const Eigen::Vector4d& quat)
{
	Eigen::Quaterniond q(quat);

	q.normalize(); // Just in case

	return q.toRotationMatrix().eulerAngles(2, 1, 0);
}

} // namespace MuJoCo
