#include <MuJoCoTools/MuJoCoFigure.h>

#include <string.h>			// Use C library too

// Default constructor
MuJoCoFigure::MuJoCoFigure() :
		MuJoCoFigure("Figure", 1)
{
	// Empty, see initializer list
}

// Constructor
MuJoCoFigure::MuJoCoFigure(const std::string& name, int n_lines)
{
	mjv_defaultFigure(&figure_);
	figure_.figurergba[3] = 0.5f;

	// set flags
	figure_.flg_extend = 1;
	figure_.flg_barplot = 0;
	figure_.flg_symmetric = 1;

	// title
	strcpy(figure_.title, name.c_str());

	// y-tick nubmer format
	strcpy(figure_.yformat, "%.1f");
	strcpy(figure_.xformat, "%.1f");

	// grid size
	figure_.gridsize[0] = 2;
	figure_.gridsize[1] = 3;

	// minimum range
	figure_.range[0][0] = 0.0f;
	figure_.range[0][1] = 0.0f;
	figure_.range[1][0] = -1.0f;
	figure_.range[1][1] = 1.0f;

	num_points_ = 200;

	num_lines_ = n_lines;

	clear(); // Clear figure

	// Default viewport
	x_ = 0;
	y_ = 0;
	height_ = 300;
	width_ = 300;
}

// Return viewport from figure
mjrRect MuJoCoFigure::get_viewport() const
{
	return { x_, y_, width_, height_ };
}

// Set legends
MuJoCoFigure& MuJoCoFigure::set_legends(const std::vector<std::string>& legends)
{
	for (int i = 0; i < num_lines_; i++)
	{
		strcpy(figure_.linename[i], legends[i].c_str());
	}
	return *this;
}

// Set a single legend
MuJoCoFigure& MuJoCoFigure::set_legend(const std::string& legend)
{
	return set_legends( { legend });
}

// Set x-axis label
MuJoCoFigure& MuJoCoFigure::set_x_label(const std::string& label)
{
	strcpy(figure_.xlabel, label.c_str());
	return *this;
}

// Set size in screen
MuJoCoFigure& MuJoCoFigure::set_size(int w, int h)
{
	width_ = w;
	height_ = h;
	return *this;
}

// Set position in screen
MuJoCoFigure& MuJoCoFigure::set_pos(int x, int y)
{
	this->x_ = x;
	this->y_ = y;
	return *this;
}

// Return mjFigure pointer
mjvFigure* MuJoCoFigure::get_figure()
{
	return &figure_;
}

// Get position in screen
void MuJoCoFigure::get_pos(int& x_res, int& y_res)
{
	x_res = x_;
	y_res = y_;
}

// Get screen size
void MuJoCoFigure::get_size(int& width_res, int& height_res)
{
	width_res = width_;
	height_res = height_;
}

// Fill figure with empty data
void MuJoCoFigure::clear()
{
	for (int l = 0; l < num_lines_; l++)
	{
		figure_.linewidth[l] = 1.0f; // line width

		for (int i = 0; i < mjMAXLINEPNT; i++)
		{
			// Write 0 to line
			figure_.linedata[l][2 * i + 0] = -1.0f / 60.0f * (float) i;	// x
			figure_.linedata[l][2 * i + 1] = 0.0f;		// y
		}
	}
}

// Set new value in figure
void MuJoCoFigure::add_value(double time, double val, int line_id)
{
	/*
	 * Data array is built like [x1 y1 x2 y2 x3 y3 ...]
	 * We use it such that the first element is the most recent one
	 */

	int pnt = mjMIN(num_points_ + 1, figure_.linepnt[line_id] + 1);

	// Scooch data
	for (int i = pnt - 1; i > 0; i--)
	{
		// Scooch x data two places (two because of x and y data)
		figure_.linedata[line_id][2 * i + 1] =
				figure_.linedata[line_id][2 * i - 1];
		figure_.linedata[line_id][2 * i] = figure_.linedata[line_id][2 * i - 2];
		// x[2] = x[0], etc.
	}

	figure_.linepnt[line_id] = pnt;
	figure_.linedata[line_id][0] = static_cast<float>(time);
	figure_.linedata[line_id][1] = static_cast<float>(val);
}

// Set new values in figure
void MuJoCoFigure::add_values(double time, const double* values, int n_values)
{
	if (n_values < 0)
	{
		n_values = num_lines_;
	}

	for (int i = 0; i < n_values; i++)
	{
		add_value(time, values[i], i);
	}
}
