#include <MuJoCoTools/MuJoCoModel.h>
#include <MuJoCoTools/MuJoCoFunctions.h>

#include <mujoco/mujoco.h>

#include <iostream>
#include <cassert>

using namespace MuJoCo;

/*
 * Initialize activated static property
 */
int MuJoCoModel::activated = 0;

// Constructor
MuJoCoModel::MuJoCoModel(const std::string& model_file)
{
	// Activate only if the first
	if (MuJoCoModel::activated == 0)
	{
	    std::string key_path;
	    if (get_activation_key_path(key_path))
        {
            mj_activate(key_path.c_str());
        }
	}
	MuJoCoModel::activated++;

	char load_error[256];
	m_ = mj_loadXML(model_file.c_str(), nullptr, load_error, 256);

	file_ = model_file;

	if (!m_)
	{
		std::cout << "Failed to load model `" << model_file << "`" << std::endl;
		std::cout << "Error: " << load_error << std::endl;
	}

	d_ = mj_makeData(m_); // Initialize data pointer

	// Allocate memory for assisting variables
	qacc_center_ = mj_stackAlloc(d_, m_->nv);
	qacc_warmstart_ = mj_stackAlloc(d_, m_->nv);

	eps_ = 1e-6; // Good value

	// Search for quaternions
	for (int i = 0; i < m_->njnt; i++)
	{
		if (m_->jnt_type[i] == mjJNT_BALL)
		{
			quat_ids_.push_back(m_->jnt_qposadr[i]);
		}
		else if (m_->jnt_type[i] == mjJNT_FREE)
		{
			// Free joint is [position, quaterion]
			quat_ids_.push_back(m_->jnt_qposadr[i] + 3);
		}
	}
}

// Copy constructor
MuJoCoModel::MuJoCoModel(const MuJoCoModel& rhs)
{
	// Copy model and data
	m_ = mj_copyModel(NULL, rhs.get_model());

	d_ = mj_makeData(m_);

	// Variables
	file_ = rhs.file_;
	quat_ids_ = rhs.quat_ids_;
	eps_ = rhs.eps_;

	// Allocate memory for assisting variables
	qacc_center_ = mj_stackAlloc(d_, m_->nv);
	qacc_warmstart_ = mj_stackAlloc(d_, m_->nv);

	MuJoCoModel::activated++; // Register new object (no point in activating)
}

// Destructor
MuJoCoModel::~MuJoCoModel()
{
	// Shut down
	mj_deleteData(d_);
	mj_deleteModel(m_);

	MuJoCoModel::activated--; // Decrement list

	// Deactivate if last
	if (MuJoCoModel::activated == 0)
	{
		mj_deactivate();
	}
}

// Assignment operator
MuJoCoModel& MuJoCoModel::operator=(const MuJoCoModel& rhs)
{
	// Check if not the same object
	if (this != &rhs)
	{
		// If objects are not of the same model
		if (file_ != rhs.file_)
		{
			// Free old objects
			mj_deleteData(d_);
			mj_deleteModel(m_);

			// Copy new ones
			m_ = mj_copyModel(NULL, rhs.get_model());

			d_ = mj_makeData(m_);

			// Allocate memory for assisting variables
			qacc_center_ = mj_stackAlloc(d_, m_->nv);
			qacc_warmstart_ = mj_stackAlloc(d_, m_->nv);

			file_ = rhs.file_;
		}

		d_ = mj_copyData(d_, m_, rhs.get_data());

		// Variables
		quat_ids_ = rhs.quat_ids_;
		eps_ = rhs.eps_;
	}

	return *this;
}

// Get XML file
const std::string& MuJoCoModel::get_file() const
{
	return file_;
}

// Return number of bodies in model
int MuJoCoModel::get_mumber_of_bodies() const
{
	return m_->nbody;
}

// Get default qpos from model
MuJoCoModel::VectorXd MuJoCoModel::get_default_qpos() const
{
	return mujoco_to_eigen(m_->qpos0, m_->nq);
}

// Enable / disable contact
void MuJoCoModel::enable_contact(bool enable_contact)
{
	if (enable_contact)
	{
		// Set disable flag to low
		m_->opt.disableflags &= ~mjDSBL_CONTACT;
	}
	else
	{
		// Set disable flag to high
		m_->opt.disableflags |= mjDSBL_CONTACT;
	}
}

// Get forward dynamics
MuJoCoModel::VectorXd MuJoCoModel::get_dynamics() const
{
	// Copy acceleration to output
	return mujoco_to_eigen(d_->qacc, m_->nv);
}

// Get derivative of acceleration w.r.t. position
MuJoCoModel::MatrixXd MuJoCoModel::get_dynamics_diff_qpos()
{
	MatrixXd qacc_diff_qpos(m_->nv, m_->nq);

	finite_difference_dynamics(d_->qpos, m_->nq, DIFF_POS, qacc_diff_qpos.data());

	return qacc_diff_qpos;
}

// Get derivative of acceleration w.r.t. velocity
MuJoCoModel::MatrixXd MuJoCoModel::get_dynamics_diff_qvel()
{
	MatrixXd qacc_diff_qvel(m_->nv, m_->nv);

	finite_difference_dynamics(d_->qvel, m_->nv, DIFF_VEL, qacc_diff_qvel.data());

	return qacc_diff_qvel;
}

// Get derivative of acceleration w.r.t. input force
MuJoCoModel::MatrixXd MuJoCoModel::get_dynamics_diff_u()
{
	MatrixXd qacc_diff_u(m_->nv, m_->nu);

	finite_difference_dynamics(d_->ctrl, m_->nu, DIFF_TORQUE, qacc_diff_u.data());

	return qacc_diff_u;
}

// Get derivative of acceleration w.r.t. cartesian forces
MuJoCoModel::MatrixXd MuJoCoModel::get_dynamics_diff_xfrc()
{
	MatrixXd qacc_diff_xfrc(m_->nv, m_->nbody * 6);

	finite_difference_dynamics(d_->xfrc_applied, m_->nbody * 6, DIFF_TORQUE,
			qacc_diff_xfrc.data());

	return qacc_diff_xfrc;
}

// Get derivative of acceleration w.r.t. a segment of cartesian forces
MuJoCoModel::MatrixXd MuJoCoModel::get_dynamics_diff_xfrc(int body_id,
		FRC_TYPE type)
{
	MatrixXd qacc_diff_xfrc(m_->nv, 3);

	int idx = body_id * 6 + type * 3;
	double* target = &d_->xfrc_applied[idx];
	finite_difference_dynamics(target, 3, DIFF_TORQUE, qacc_diff_xfrc.data());

	return qacc_diff_xfrc;
}

// Take finite difference derivative of qacc to some target
void MuJoCoModel::finite_difference_dynamics(double* target, int var_size,
		DIFF_TYPE type, double* qacc_diff)
{
	// Copy the regular (center) output and put them aside
	mju_copy(qacc_center_, d_->qacc, m_->nv);
	mju_copy(qacc_warmstart_, d_->qacc_warmstart, m_->nv);

	double original[var_size];
	mju_copy(original, target, var_size);

	// Loop over variables to be varied
	for (int i_var = 0; i_var < var_size; i_var++)
	{
		target[i_var] += eps_; // Perturb current variable

		int quat_id = -1;

		// Check of part of quaternion
		if (type == DIFF_POS)
		{
			mj_normalizeQuat(m_, target);
			quat_id = get_quaternion_index(i_var);
		}

		if (quat_id >= 0)
		{
			// Re-normalize quaternion after perturbation
			mju_normalize4(&target[quat_id]);
		}

		// Place default warmstart to speed up this computation iteration
		mju_copy(d_->qacc_warmstart, qacc_warmstart_, m_->nv);

		// TODO: Use skipstage for more speed
		mj_forward(m_, d_);

		// Loop through columns of qacc
		for (int c = 0; c < m_->nv; c++)
		{
			// Take finite difference
			double derivative = (d_->qacc[c] - qacc_center_[c]) / eps_;

			// Place inside derivative vector / matrix
			qacc_diff[i_var * m_->nv + c] = derivative;
		}

		// Restore target
		//mju_copy(target, original, var_size);
		if (quat_id < 0)
		{
			target[i_var] = original[i_var]; // Undo perturbation
		}
		else
		{
			mju_copy4(&target[quat_id], &original[quat_id]);
		}
	}

	/**
	 * With the original states restored, compute model again to make
	 * sure everything is back as the start
	 */
	mj_forward(m_, d_);
}

// Return whether a given qpos index is part of a quaternion
int MuJoCoModel::get_quaternion_index(int i_q) const
{
	for (auto quat_id : quat_ids_)
	{
		if (i_q >= quat_id && i_q < quat_id + 4)
		{
			return quat_id;
		}
	}

	return -1;
}

// Get global 3D position of body (at the body frame)
MuJoCoModel::Vector3d MuJoCoModel::get_position(int body_id) const
{
	assert(
			body_id >= 0 && body_id < m_->nbody
					&& "body_id must be zero or higher and less then the number of bodies");

	return mujoco_to_eigen(&d_->xpos[body_id * 3], 3);
}

// Get global orientation (quaternion) of body
MuJoCoModel::Vector4d MuJoCoModel::get_orientation(int body_id) const
{
	assert(
			body_id >= 0 && body_id < m_->nbody
					&& "body_id must be zero or higher and less then the number of bodies");

	return mujoco_to_eigen(&d_->xquat[body_id * 4], 4);
}

// Get geometric jacobian of body for position
MuJoCoModel::MatrixXd MuJoCoModel::get_geometric_jacobian_pos(int body_id) const
{
	MatrixXd jac(3, m_->nv);

	double jac_rm[m_->nv * 3];

	mj_jacBody(m_, d_, jac_rm, nullptr, body_id);

	rowMmjor_to_colmajor(jac_rm, jac.data(), 3, m_->nv);

	return jac;
}

// Get geometric jacobian of body for orientation
MuJoCoModel::MatrixXd MuJoCoModel::get_geometric_jacobian_rot(int body_id) const
{
	MatrixXd jac(3, m_->nv);

	double jac_rm[m_->nv * 3];

	mj_jacBody(m_, d_, nullptr, jac_rm, body_id);

	rowMmjor_to_colmajor(jac_rm, jac.data(), 3, m_->nv);

	return jac;
}

// Get derivative of body position to qpos using finite difference
MuJoCoModel::MatrixXd MuJoCoModel::get_position_diff_qpos(int body_id)
{
	const int var_size = m_->nq; // Size of qpos

	MatrixXd pos_diff_qpos(3, var_size);

	mjtNum pos_center[3];
	mju_copy3(pos_center, &d_->xpos[body_id * 3]);

	mjtNum original[var_size];
	mju_copy(original, d_->qpos, var_size);

	// Loop over variables to be varied
	for (int i_var = 0; i_var < var_size; i_var++)
	{
		d_->qpos[i_var] += eps_; // Perturb current variable

		if (!quat_ids_.empty())
		{
			mj_normalizeQuat(m_, d_->qpos);
		}

		// Compute kinematics
		mj_kinematics(m_, d_);

		// Loop through columns of pos
		for (int c = 0; c < 3; c++)
		{
			mjtNum pos_perturbed_c = d_->xpos[body_id * 3 + c];

			// Take finite difference
			double derivative = (pos_perturbed_c - pos_center[c]) / eps_;

			// Place inside derivative vector / matrix
			pos_diff_qpos(c, i_var) = derivative;
		}

		mju_copy(d_->qpos, original, var_size);
	}

	mj_kinematics(m_, d_); // Reset kinematics

	return pos_diff_qpos;
}

// Get sum of all contact forces on a specified body
MuJoCoModel::Vector3d MuJoCoModel::get_contact_force(int body_id)
{
	Vector3d force = Vector3d::Zero();

	for (int i = 0; i < d_->ncon; i++)
	{
		const mjContact* con = &d_->contact[i];
		if (m_->geom_bodyid[con->geom1] == body_id
				|| m_->geom_bodyid[con->geom2] == body_id)
		{
			double wrench_local[6];
			mj_contactForce(m_, d_, i, wrench_local);
			double force_local[3];
			mju_rotVecMatT(force_local, wrench_local, con->frame);
			for (int c = 0; c < 3; c++)
			{
				force[c] += force_local[c];
			}
		}
	}

	return force;
}

// Use inverse dynamics to compute generalized
MuJoCoModel::VectorXd MuJoCoModel::get_inverse_dynamics(
		const VectorXd& qacc_ref)
{
	// Write target acceleration
	eigen_copy(d_->qacc, qacc_ref, m_->nv);

	// Perform inverse dynamics
	mj_inverse(m_, d_);

	return mujoco_to_eigen(d_->qfrc_inverse, m_->nv);
}

// Inverse kinematics (for base and end-effector)
bool MuJoCoModel::inverse_kinematics(int base_id, const Vector3d& base_pos,
		int ee_id, const Vector3d& ee_pos)
{
	return inverse_kinematics( { { base_id, base_pos }, { ee_id, ee_pos } });
}

// Inverse kinematics (for list of bases and end-effectors)
bool MuJoCoModel::inverse_kinematics(const ListVector3d& refs_pos,
		const ListVector4d& refs_quat, const IKSettings& settings)
{
	const int n_pos = refs_pos.size();
	const int n_quat = refs_quat.size();
	const int n = n_pos + n_quat;

	for (int i = 0; i < settings.iter_max; i++)
	{
		bool done = true;

		MatrixXd J_combined(n * 3, m_->nv);
		VectorXd error_combined(n * 3);

		int i_stack = 0;

		for (auto ref : refs_pos) // Position
		{
			int body_id = ref.first;
			Vector3d error = ref.second - get_position(body_id);

			if (error.norm() > settings.pos_threshold)
			{
				done = false;
			}
			MatrixXd J = get_geometric_jacobian_pos(body_id);

			J_combined.middleRows(i_stack * 3, 3) = J;
			error_combined.middleRows(i_stack * 3, 3) = error
					* settings.pos_eps;
			i_stack++;
		}

		for (auto ref : refs_quat) // Quaternion
		{
			int body_id = ref.first;
			Vector4d orientation = get_orientation(body_id);
			Vector3d error; // Error is expressed like angular velocity
			mju_subQuat(error.data(), ref.second.data(), orientation.data());

			if (error.norm() > settings.quat_threshold)
			{
				done = false;
			}
			MatrixXd J = get_geometric_jacobian_rot(body_id);

			J_combined.middleRows(i_stack * 3, 3) = J;
			error_combined.middleRows(i_stack * 3, 3) = error
					* settings.quat_eps;
			i_stack++;
		}

		if (done)
		{
			return true;
		}

		// Compute joint velocities
		VectorXd v = psuedo_inverse_mult(J_combined, error_combined);

		// Integrate to joint positions
		mj_integratePos(m_, d_->qpos, v.data(), 1.0);
		mj_forward(m_, d_);
	}

	return false;
}

// Get torque from inputs in generalized coordinates (nv x 1)
MuJoCoModel::VectorXd MuJoCoModel::get_actuation_torque() const
{
	return mujoco_to_eigen(d_->qfrc_actuator, m_->nv);
}

// Get jacobian of input torques to generalized torques (nv x nu)
MuJoCoModel::MatrixXd MuJoCoModel::get_actuation_jacobian()
{
	MatrixXd jac(m_->nv, m_->nu);

	const int var_size = m_->nu; // Size of ctrl

	mjtNum qfrc_center[m_->nv];
	mju_copy(qfrc_center, d_->qfrc_actuator, m_->nv);

	// Loop over variables to be varied
	for (int i_var = 0; i_var < var_size; i_var++)
	{
		double original = d_->ctrl[i_var];

		d_->ctrl[i_var] += eps_; // Perturb current variable

		// Perform torque forward operation
		mj_fwdActuation(m_, d_);

		// Loop through columns
		for (int c = 0; c < m_->nv; c++)
		{
			mjtNum qfrc_perturbed_c = d_->qfrc_actuator[c];

			// Take finite difference
			double derivative = (qfrc_perturbed_c - qfrc_center[c]) / eps_;

			// Place inside derivative vector / matrix
			jac(c, i_var) = derivative;
		}

		d_->ctrl[i_var] = original;
	}

	mj_fwdActuation(m_, d_); // Reset actuation

	return jac;
}

// Return map of heightfield
MuJoCoModel::MatrixXd MuJoCoModel::get_heightfield(int id, bool shape_only)
{
	if (id >= m_->nhfield)
	{
		// hfield does not exist
		return MatrixXd(0, 0);
	}

	MatrixXd map(m_->hfield_nrow[id], m_->hfield_ncol[id]);

	if (shape_only)
	{
		return map;
	}

	int h_i = m_->hfield_adr[id];
	const double height = m_->hfield_size[id * 4 + 2];

	// HField data is row-major
	for (int r = 0; r < map.rows(); r++)
	{
		for (int c = 0; c < map.cols(); c++)
		{
			map(r, c) = m_->hfield_data[h_i++] * height;
		}
	}

	return map;
}

// Set height data of an existing heightfield
bool MuJoCoModel::set_heightfield_normalized(int id, const MatrixXd& map,
		double max_elevation, double radius_x, double radius_y, double base_z)
{
	if (id >= m_->nhfield)
	{
		// hfield does not exist
		return false;
	}
	int nele = m_->hfield_ncol[id] * m_->hfield_nrow[id];
	if (nele != map.size())
	{
		// Incorrect size
		return false;
	}

	int h_i = m_->hfield_adr[id];

	// HField data is row-major
	for (int r = 0; r < map.rows(); r++)
	{
		for (int c = 0; c < map.cols(); c++)
		{
			m_->hfield_data[h_i++] = map(r, c);
		}
	}

	if (max_elevation > 0.0)
	{
		m_->hfield_size[id * 4 + 2] = max_elevation;
	}
	if (radius_x > 0.0 && radius_y > 0.0)
	{
		m_->hfield_size[id * 4 + 0] = radius_x;
		m_->hfield_size[id * 4 + 1] = radius_y;
	}
	if (base_z > 0.0)
	{
		m_->hfield_size[id * 4 + 3] = base_z;
	}

	return true;
}

// Set height data of an existing heightfield
bool MuJoCoModel::set_heightfield(int id, MatrixXd map, double radius_x,
		double radius_y, double base_z)
{
	const double max_height = map.maxCoeff();

	if (max_height > 1e-5)
	{
		map *= (1.0 / max_height); // Normalize matrix
	}

	return set_heightfield_normalized(id, map, max_height, radius_x, radius_y,
			base_z);
}

// Set map to geom
bool MuJoCoModel::set_terrain(int id, MatrixXd map, double radius_x,
		double radius_y, double base_z)
{
	assert(id >= 0 && id < m_->ngeom && "Invalid Geom id");

	int hfield_id = m_->geom_dataid[id];

	if (hfield_id < 0)
	{
		return false; // No hfield linked
	}

	const double min_height = map.minCoeff();
	map = map.array() - min_height;
	m_->geom_pos[id * 3 + 2] = min_height;

	return set_heightfield(hfield_id, map, radius_x, radius_y, base_z);
}

// Set current state of MJ data object
void MuJoCoModel::reset_data(const VectorXd& qpos, const VectorXd& qvel,
		const VectorXd& u, const ListVectorXd& xfrc)
{
	// Copy state and input to data object

	if (qpos.size() > 0)
	{
		eigen_copy(d_->qpos, qpos, m_->nq);

		if (!quat_ids_.empty())
		{
			// Normalize quaternions
			mj_normalizeQuat(m_, d_->qpos);
		}
	}

	if (qvel.size() > 0)
	{
		eigen_copy(d_->qvel, qvel, m_->nv);
	}

	if (u.size() > 0)
	{
		eigen_copy(d_->ctrl, u, m_->nu);
	}

	if (!xfrc.empty())
	{
		VectorXd xfrc_full(m_->nbody * 6);
		xfrc_full.setZero();

		for (auto xfrc_pair : xfrc)
		{
			int body_id = xfrc_pair.first;
			xfrc_full.segment(body_id * 6, 6) = xfrc_pair.second;
		}

		eigen_copy(d_->xfrc_applied, xfrc_full);
	}

	/*
	 * Call forward function to compute accelerations, positions, velocities
	 * but also jacobians.
	 */
	mj_forward(m_, d_);
}

// Get joint limits corresponding to qpos vector
std::vector<std::pair<double, double>> MuJoCoModel::get_joint_limits() const
{
	std::vector<std::pair<double, double>> limits(m_->nq, { -1.0e19, 1.0e19 }); // Initialize without limits

	for (int i = 0; i < m_->njnt; i++)
	{
		int i_q = m_->jnt_qposadr[i];

		if (m_->jnt_type[i] == mjJNT_BALL || m_->jnt_type[i] == mjJNT_FREE)
		{
			if (m_->jnt_type[i] == mjJNT_FREE)
				i_q += 3; // Skip position part

			for (int j = 0; j < 3; j++)
				limits[i_q + j] = std::make_pair(-1.0, 1.0); // Quaternion limits

			continue;
		}

		if (m_->jnt_limited[i])
		{
			double min = m_->jnt_range[2 * i], max = m_->jnt_range[2 * i + 1];
			limits[i_q] = std::make_pair(min, max);
		}
	}

	return limits;
}

// Get actuator limits
std::vector<std::pair<double, double>> MuJoCoModel::get_torque_limits() const
{
	std::vector<std::pair<double, double>> limits(m_->nu, { -1.0e19, 1.0e19 }); // Initialize without limits

	for (int i = 0; i < m_->nu; i++)
	{
		if (m_->actuator_ctrllimited[i])
		{
			double min = m_->actuator_ctrlrange[2 * i], max = m_->actuator_ctrlrange[2 * i + 1];
			limits[i] = std::make_pair(min, max);
		}
	}

	return limits;
}

// Return list of quaternion ids
const std::vector<int>& MuJoCoModel::get_quat_ids() const
{
	return quat_ids_;
}

// Turn row-major matrix into a column-major matrix (as arrays)
void MuJoCoModel::rowMmjor_to_colmajor(const double* mat_rm, double* mat_cm,
		int rows, int cols) const
{
	// Convert to column-major
	for (int row = 0; row < rows; row++)
	{
		for (int col = 0; col < cols; col++)
		{
			mat_cm[col * rows + row] = mat_rm[row * cols + col];
		}
	}
}

// Return pointer to owned mjModel object
mjModel* MuJoCoModel::get_model() const
{
	return m_;
}

// Return pointer to owned mjData object
mjData* MuJoCoModel::get_data() const
{
	return d_;
}

// Get id of body
int MuJoCoModel::get_body_id(const std::string& name) const
{
	return mj_name2id(m_, mjOBJ_BODY, name.c_str());
}

// Default IKSettings constructor
MuJoCoModel::IKSettings::IKSettings()
{
	iter_max = 30;
	pos_eps = 0.3;
	quat_eps = 0.3;
	pos_threshold = 0.01; // 1 cm
	quat_threshold = 0.05; // rad
}

// IKSettings constructor
MuJoCoModel::IKSettings::IKSettings(int i_max, double p_eps, double q_eps,
		double p_threshold, double q_threshold)
{
	iter_max = i_max;
	pos_eps = p_eps;
	quat_eps = q_eps;
	pos_threshold = p_threshold;
	quat_threshold = q_threshold;
}
