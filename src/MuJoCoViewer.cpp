#include <mujoco/mujoco.h>
#include <mujoco/glfw3.h>

#include <MuJoCoTools/MuJoCoViewer.h>

#include <iostream>
#include <string.h>
#include <unistd.h>

/*
 * Initialize initialized static property
 */
bool MuJoCoViewer::initialized = false;

// Constructor
MuJoCoViewer::MuJoCoViewer(const mjModel* m)
{
	if (m)
	{
		initialize(m);
	}
	else
	{
		this->m_ = nullptr;
	}

	figures_ = { };
}

// Constructor
MuJoCoViewer::MuJoCoViewer(const MuJoCoModel& model) :
		MuJoCoViewer(model.get_model())
{
	// Rely on initializer list
}

// Destructor
MuJoCoViewer::~MuJoCoViewer()
{
	glfwDestroyWindow(window_);

	mjv_freeScene(&scn_);
	mjr_freeContext(&con_);

	// Terminate GLFW (crashes with Linux NVidia drivers)
#if defined(__APPLE__) || defined(_WIN32)
    glfwTerminate();
#endif
}

// Initialize
void MuJoCoViewer::initialize(const mjModel* m)
{
	if (!MuJoCoViewer::initialized)
	{
		if (!glfwInit())
		{
			std::cout << "Failed to initialise GLFW" << std::endl;
		}
		MuJoCoViewer::initialized = true;
	}

	// Create window, make OpenGL context current, request v-sync
	window_ = glfwCreateWindow(1200, 900, "MuJoCo Viewer", NULL, NULL);

	glfwMakeContextCurrent(window_);
	glfwSwapInterval(1);

	// Initialise visualisation data structures
	mjv_defaultCamera(&cam_);
	mjv_defaultOption(&opt_);
	mjv_defaultScene(&scn_);
	mjr_defaultContext(&con_);

	// Place pointer to this object inside the window reference
	glfwSetWindowUserPointer(window_, this);

	// Install GLFW mouse and keyboard callbacks
	glfwSetKeyCallback(window_, cb_keyboard);
	glfwSetCursorPosCallback(window_, cb_mouse_move);
	glfwSetMouseButtonCallback(window_, cb_mouse_button);
	glfwSetScrollCallback(window_, cb_scroll);

	// Initialize controls
	button_left_ = button_middle_ = button_right_ = false;
	mouse_x_ = mouse_y_ = 0.0;

	this->m_ = m;

	// Create scene and context
	mjv_makeScene(m, &scn_, 1000);
	mjr_makeContext(m, &con_, mjFONTSCALE_150);

	// Set window title
	if (m->names)
	{
		char title[200] = "MuJoCo : ";
		strcat(title, m->names);
		glfwSetWindowTitle(window_, title);
	}
}

// Return true when window was closed through OS
bool MuJoCoViewer::window_should_close() const
{
	return glfwWindowShouldClose(window_);
}

// Update mjScene
void MuJoCoViewer::update_scene(mjData* d)
{
	mjv_updateScene(m_, d, &opt_, NULL, &cam_, mjCAT_ALL, &scn_);
}

// Render new scene and poll for glwfw events
void MuJoCoViewer::update(mjData* d)
{
	// Get framebuffer viewport
	mjrRect viewport = { 0, 0, 0, 0 };
	glfwGetFramebufferSize(window_, &viewport.width, &viewport.height);

	// Update scene and render
	if (d)
	{
		update_scene(d);
	}
	mjr_render(viewport, &scn_, &con_);

	// Render figure(s)
	for (auto figure : figures_)
	{
		mjr_figure(figure->get_viewport(), figure->get_figure(), &con_);
	}

	// swap OpenGL buffers (blocking call due to v-sync)
	glfwSwapBuffers(window_);

	// Process events and run callbacks
	glfwPollEvents();
}

// Run simulation until window is closed
void MuJoCoViewer::play(mjData* d)
{
	double simstart = glfwGetTime();

	// Repeat simulation
	while (!window_should_close())
	{
		// Record cpu time at start of iteration
		double tmstart = glfwGetTime();

		update(d); // Render

		while (d->time < (tmstart - simstart))
		{
			mj_step(m_, d);
		}

		// Wait until a total of 1 frame time has passed
		while (glfwGetTime() - tmstart < 1.0 / 60.0)
		{
			usleep(500); // Sleep for 0.5 millisecond to prevent a busy wait
		}
	}
}

// Callback for keyboard event
void MuJoCoViewer::cb_keyboard(GLFWwindow*, int, int,
		int, int)
{
	// Do nothing
	//auto my_this = MuJoCoViewer::getViewerPtr(window);
}

// Callback for mouse buttons
void MuJoCoViewer::cb_mouse_button(GLFWwindow* window, int, int,
		int)
{
	auto viewer = MuJoCoViewer::get_viewer_ptr(window);

	// Update button state
	viewer->button_left_ = (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT)
			== GLFW_PRESS);
	viewer->button_middle_ = (glfwGetMouseButton(window,
	GLFW_MOUSE_BUTTON_MIDDLE) == GLFW_PRESS);
	viewer->button_right_ = (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT)
			== GLFW_PRESS);

	// Update mouse position
	glfwGetCursorPos(window, &(viewer->mouse_x_), &(viewer->mouse_y_));
}

// Callback for mouse move
void MuJoCoViewer::cb_mouse_move(GLFWwindow* window, double xpos, double ypos)
{
	auto viewer = MuJoCoViewer::get_viewer_ptr(window);

	// No buttons down: nothing to do
	if (!viewer->button_left_ && !viewer->button_middle_ && !viewer->button_right_)
	{
		return;
	}

	// Compute mouse displacement, save
	double dx = xpos - viewer->mouse_x_;
	double dy = ypos - viewer->mouse_y_;
	viewer->mouse_x_ = xpos;
	viewer->mouse_y_ = ypos;

	// Get current window size
	int width, height;
	glfwGetWindowSize(window, &width, &height);

	// Get shift key state
	bool mod_shift = (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS
			|| glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS);

	// determine action based on mouse button
	mjtMouse action;
	if (viewer->button_right_)
	{
		action = mod_shift ? mjMOUSE_MOVE_H : mjMOUSE_MOVE_V;
	}
	else if (viewer->button_left_)
	{
		action = mod_shift ? mjMOUSE_ROTATE_H : mjMOUSE_ROTATE_V;
	}
	else
	{
		action = mjMOUSE_ZOOM;
	}

	// Move camera
	mjv_moveCamera(viewer->m_, action, dx / height, dy / height, &(viewer->scn_),
			&(viewer->cam_));
}

// Callback for scrolling
void MuJoCoViewer::cb_scroll(GLFWwindow* window, double, double yoffset)
{
	auto viewer = MuJoCoViewer::get_viewer_ptr(window);

	// Emulate vertical mouse motion = 5% of window height
	mjv_moveCamera(viewer->m_, mjMOUSE_ZOOM, 0, 0.05 * yoffset, &(viewer->scn_),
			&(viewer->cam_));
}

// Get MuJoCoViewer pointer from window object
MuJoCoViewer* MuJoCoViewer::get_viewer_ptr(GLFWwindow* window)
{
	return static_cast<MuJoCoViewer*>(glfwGetWindowUserPointer(window));
}

// Create arrow with size of given vector, starting from a base
void MuJoCoViewer::draw_arrow(const double vector[3], const double base[3])
{
	// Check if there is space left
	if (scn_.ngeom >= scn_.maxgeom)
	{
		return;
	}

	mjvGeom* arrow = scn_.geoms + scn_.ngeom;

	// Create geom
	arrow->type = mjGEOM_ARROW;
	arrow->dataid = -1;
	arrow->objtype = mjOBJ_SITE;
	arrow->objid = -1;
	arrow->category = mjCAT_DECOR;
	arrow->texid = -1;
	arrow->texuniform = 0;
	arrow->texrepeat[0] = 1;
	arrow->texrepeat[1] = 1;
	arrow->emission = 0;
	arrow->specular = 0.5;
	arrow->shininess = 0.5;
	arrow->reflectance = 0;
	arrow->label[0] = 0;
	arrow->rgba[0] = 1.0f;
	arrow->rgba[1] = 0.1f;
	arrow->rgba[2] = 0.1f;
	arrow->rgba[3] = 1.0f;

	arrow->pos[0] = (float) base[0];
	arrow->pos[1] = (float) base[1];
	arrow->pos[2] = (float) base[2];

	float magnitude = (float) mju_norm3(vector);
	arrow->size[0] = 0.02f;
	arrow->size[1] = arrow->size[0];
	arrow->size[2] = magnitude;

	mjtNum quat[4], mat[9];
	mju_quatZ2Vec(quat, vector);
	mju_quat2Mat(mat, quat);
	mju_n2f(arrow->mat, mat, 9);

	// Register new geom
	scn_.ngeom++;
}

// Link new viewer
void MuJoCoViewer::add_figure(MuJoCoFigure* figure)
{
	// Automatically stack figures
	int x, y;
	figure->get_pos(x, y);
	if (x == 0)
	{
		y = 0;
		if (!figures_.empty())
		{
			for (auto fig : figures_)
			{
				int width, height;
				fig->get_size(width, height);
				y += height;
			}
		}
		figure->set_pos(x, y);
	}
	figures_.push_back(figure);
}

// Let current thread wait for an amount of seconds
void MuJoCoViewer::sleep_seconds(double seconds)
{
	long us = static_cast<long>(1.0e6 * seconds);
	auto duration = std::chrono::microseconds(us);
	std::this_thread::sleep_for(duration);
}
